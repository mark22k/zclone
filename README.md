# zclone

Git wrapper script to clone Git repositories from ZeroNet.

Inspired by [zclone](http://127.0.0.1:43110/1GitLiXB6t5r8vuU2zC6a8GYj9ME6HMQ4t/repo/?1JFRoDXLLGXuiDE7VzccH3cbeeAkE3K499) written in Free Pascal.
