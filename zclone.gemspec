# frozen_string_literal: true
# sharable_constant_value: literal

Gem::Specification.new do |spec|
  spec.name = 'zclone'
  spec.version = '1.0.0'
  spec.summary = 'Git wrapper script to clone Git repositories from ZeroNet.'
  spec.description = 'Git wrapper script to clone Git repositories from ZeroNet.'
  spec.authors = ['Marek Küthe']
  spec.email = 'm.k@mk16.de'

  spec.executables = %w[zclone]
  spec.extra_rdoc_files = %w[LICENSE README.md]

  spec.homepage = 'https://codeberg.org/mark22k/zclone'
  spec.license = 'WTFPL'

  spec.metadata = { 'source_code_uri' => 'https://codeberg.org/mark22k/zclone',
                    'bug_tracker_uri' => 'https://codeberg.org/mark22k/zclone/issues',
                    'rubygems_mfa_required' => 'true' }

  spec.required_ruby_version = '>= 3.1'
end
